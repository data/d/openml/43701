# OpenML dataset: Contraceptive-Method-Choice

https://www.openml.org/d/43701

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

1. Title: Contraceptive Method Choice
2. Sources:
(a) Origin:  This dataset is a subset of the 1987 National Indonesia
                Contraceptive Prevalence Survey
(b) Creator: Tjen-Sien Lim (limtstat.wisc.edu)
(c) Donor:   Tjen-Sien Lim (limtstat.wisc.edu)
(d) Date:    June 7, 1997
3. Past Usage:
Lim, T.-S., Loh, W.-Y.  Shih, Y.-S. (1999). A Comparison of
   Prediction Accuracy, Complexity, and Training Time of Thirty-three
   Old and New Classification Algorithms. Machine Learning. Forthcoming.
   (ftp://ftp.stat.wisc.edu/pub/loh/treeprogs/quest1.7/mach1317.pdf or
   (http://www.stat.wisc.edu/limt/mach1317.pdf)
4. Relevant Information:
This dataset is a subset of the 1987 National Indonesia Contraceptive
   Prevalence Survey. The samples are married women who were either not 
   pregnant or do not know if they were at the time of interview. The 
   problem is to predict the current contraceptive method choice 
   (no use, long-term methods, or short-term methods) of a woman based 
   on her demographic and socio-economic characteristics.
5. Number of Instances: 1473
6. Number of Attributes: 10 (including the class attribute)
7. Attribute Information:

Wife's age                     (numerical)
Wife's education               (categorical)      1=low, 2, 3, 4=high
Husband's education            (categorical)      1=low, 2, 3, 4=high
Number of children ever born   (numerical)
Wife's religion                (binary)           0=Non-Islam, 1=Islam
Wife's now working?            (binary)           0=Yes, 1=No
Husband's occupation           (categorical)      1, 2, 3, 4
Standard-of-living index       (categorical)      1=low, 2, 3, 4=high
Media exposure                 (binary)           0=Good, 1=Not good
Contraceptive method used     (class attribute)  1=No-use ,2=Long-term,3=Short-term

8. Missing Attribute Values: None

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43701) of an [OpenML dataset](https://www.openml.org/d/43701). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43701/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43701/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43701/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

